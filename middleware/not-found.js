const notFound = (req, res) =>
  res.status(404).send("Sorry, URL does not exist");

module.exports = notFound;
