const express = require("express");
const app = express();
const tasks = require("./routes/tasks");
const connectDB = require("./db/connect");
require("dotenv").config();
const notFound = require("./middleware/not-found");
const errorHandlerMiddleware = require("./middleware/error-handler");

// middleware
app.use(express.json());
app.use(express.static("./public"));

// routes
app.use("/api/v1/tasks", tasks);
app.use(notFound);
app.use(errorHandlerMiddleware);

const PORT = process.env.PORT || 8000;

const startApp = async () => {
  try {
    await connectDB(process.env.MONGO_URI);
    app.listen(PORT);
  } catch (error) {
    console.log(error);
  }
};

startApp();
